const express = require('express')
const app = express()
app.get('/', (request, response) => {
  response.send('app initializes sucessfully')
})

app.get('/show-buttons', (request, response) => {
  const { userId } = request.query

  const url = process.env.WEBVIEW_URL
  const displayUrl = `${url}/show-webview/${userId}`
  response.json({
    messages: [
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: 'generic',
            image_aspect_ratio: 'square',
            elements: [
              {
                title: `Tap the button below to select reservation date.`,

                buttons: [
                  {
                    type: 'web_url',
                    url: displayUrl,
                    title: 'Select Date',
                    messenger_extensions: true,
                    webview_height_ratio: 'compact'
                  }
                ]
              }
            ]
          }
        }
      }
    ]
  })
})
app.get('/show-webview/:userId', (req, res) => {
  const userId = req.params.userId
  res.render('webview', { data: userId })
})
app.post('/broadcast-to-chatfuel', (request, response) => {
  const botId = process.env.CHATFUEL_BOT_ID
  const chatfuelToken = process.env.CHATFUEL_TOKEN
  const userId = request.body.uId
  const blockName = 'WebviewResponse'
  const apiEndPoint = process.env.API_END_POINT
  const broadcastApiUrl = `${apiEndPoint}/bots/${botId}/users/${userId}/send`
  const query = Object.assign(
    {
      chatfuel_token: chatfuelToken,
      chatfuel_block_name: blockName
    },
    request.body
  )

  const chatfuelApiUrl = url.format({
    pathname: broadcastApiUrl,
    query
  })

  const options = {
    uri: chatfuelApiUrl,
    headers: {
      'Content-Type': 'application/json'
    }
  }

  requestPromise.post(options).then(data => {
    response.json({})
  })
})
