const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const url = require('url')
const requestPromise = require('request-promise')
const hbs = require('hbs')
const path = require('path')
const port = process.env.PORT || 3001
const index = require('./routes/routes')

app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static('public'))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')
app.use('/', index)

// listen for requests :)
const listener = app.listen(port, () => {
  console.log(`Your app is listening on port`)
})
